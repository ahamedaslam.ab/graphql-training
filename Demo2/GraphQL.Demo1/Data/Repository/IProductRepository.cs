﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;

namespace GraphQL.Demo1.Data.Repository
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAll();
        Task<Product> GetProductById(int id);
        Task<List<ProductReview>> GetReviewsByProductId(int productId);
        Task<ILookup<int, ProductReview>> GetReviewsByProductIds(IEnumerable<int> productIds);
        Task<List<SpecialProduct>> GetSpecialProducts();
    }
}
