﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL.Types
{
    public class SpecialProductType : ObjectGraphType<SpecialProduct>
    {
        public SpecialProductType()
        {
            Field(x => x.Size);
            Interface<IProductType>();
        }
    }
}
