﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Api.GraphQL.Types;
using GraphQL.Demo1.GraphQL.Messages;
using GraphQL.Resolvers;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL
{
    public class MyCommerceSubscription : ObjectGraphType
    {
        public MyCommerceSubscription(ReviewMessageService messageService)
        {
            Name = "Subscription";
            AddField(new EventStreamFieldType
            {
                Name = "reviewAdded",
                Type = typeof(ReviewAddedMessageType),
                Resolver = new FuncFieldResolver<ReviewAddedMessage>(c => c.Source as ReviewAddedMessage),
                Subscriber = new EventStreamResolver<ReviewAddedMessage>(c => messageService.GetMessages())
            });
        }
    }
}
