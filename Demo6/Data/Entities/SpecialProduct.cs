﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.Demo1.Data.Entities
{
    public class SpecialProduct: Product, IProduct
    {
        public int Size { get; set; }
    }
}
