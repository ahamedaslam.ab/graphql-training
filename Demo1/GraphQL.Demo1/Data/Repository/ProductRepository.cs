﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;

namespace GraphQL.Demo1.Data.Repository
{
    public class ProductRepository : IProductRepository
    {
        public Task<List<Product>> GetAll()
        {
            return Task.FromResult(new List<Product>
            {
                new Product{ Id= 1001, Name ="Product 1", Description = "This is the description 1 " },
                 new Product{ Id= 1002, Name ="Product 2", Description = "This is the description 2" },
                  new Product{ Id= 1003, Name ="Product 3", Description = "This is the description 3" }
            });
        }
    }
}
