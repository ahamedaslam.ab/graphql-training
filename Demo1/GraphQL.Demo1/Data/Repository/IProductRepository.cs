﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;

namespace GraphQL.Demo1.Data.Repository
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAll();
    }
}
