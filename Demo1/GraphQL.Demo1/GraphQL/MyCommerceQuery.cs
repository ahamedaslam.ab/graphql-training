﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Repository;
using GraphQL.Demo1.GraphQL.Types;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL
{
    public class MyCommerceQuery : ObjectGraphType
    {
        public MyCommerceQuery(IProductRepository productRepository)
        {
            Field<ListGraphType<ProductType>>(
                "products",
                resolve: context => productRepository.GetAll()
            );
        }
    }
}
