﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL
{
    public class MyCommerceSchema : Schema
    {
        public MyCommerceSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<MyCommerceQuery>();
        }
    }
}
