﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;
using GraphQL.Demo1.Data.Repository;
using GraphQL.Types;
using GraphQL.DataLoader;
using GraphQL.Demo1.GraphQL.Validations;

namespace GraphQL.Demo1.GraphQL.Types
{
    public class ProductType : ObjectGraphType<Product>
    {
        //public ProductType(IProductRepository productRepository)
        public ProductType(IProductRepository productRepository, IDataLoaderContextAccessor dataLoaderAccessor)
        {
            Field(t => t.Id);
            Field(t => t.Name).Description("The name of the product");
            Field(t => t.Description);
            Field(t => t.IntroducedAt).Description("When the product was first introduced in the catalog");
            Field(t => t.PhotoFileName).Description("The file name of the photo so the client can render it");
            Field(t => t.Price).PreventField();
            Field(t => t.Rating).Description("The (max 5) star customer rating");
            Field(t => t.Stock);
            Field<ProductTypeEnumType>("Type", "The type of product");

            //Field<ListGraphType<ProductReviewType>>(
            //        "reviews",
            //        resolve: context => productRepository.GetReviewsByProductId(context.Source.Id)
            //    );

            Field<ListGraphType<ProductReviewType>>(
               "reviews",
               resolve: context =>
               {
                   var loader =
                       dataLoaderAccessor.Context.GetOrAddCollectionBatchLoader<int, ProductReview>(
                           "GetReviewsByProductId", productRepository.GetReviewsByProductIds);
                   return loader.LoadAsync(context.Source.Id);
               });
        }
    }
}