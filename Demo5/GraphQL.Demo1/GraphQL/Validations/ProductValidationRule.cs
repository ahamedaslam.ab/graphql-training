﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Language.AST;
using GraphQL.Validation;

namespace GraphQL.Demo1.GraphQL.Validations
{
    public class ProductValidationRule : IValidationRule
    {
        public INodeVisitor Validate(ValidationContext context)
        {
            return new EnterLeaveListener(_ =>
            {
                _.Match<Field>(f =>
                {
                    var fieldData = context.TypeInfo.GetFieldDef();

                    if (fieldData.PreventRequired())
                    {
                        context.ReportError(new ValidationError(
              context.OriginalQuery,
              "auth-required",
              $"You are not authorized to run this query.",
              f));
                    }
                });
            });
        }
    }
}
