﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Builders;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL.Validations
{
    public static class FieldExtension
    {
        public static string PreventKey = "Prevent";
        public static void PreventField(this IProvideMetadata type)
        {
            type.Metadata[PreventKey] = true;
        }

        public static FieldBuilder<TSourceType, TReturnType> PreventField<TSourceType, TReturnType>(
            this FieldBuilder<TSourceType, TReturnType> builder)
        {
            builder.FieldType.PreventField();
            return builder;
        }

        public static bool PreventRequired(this IProvideMetadata type)
        {
            var permissions = type.GetMetadata<bool>(PreventKey, false);
            return permissions;
        }

    }
}
