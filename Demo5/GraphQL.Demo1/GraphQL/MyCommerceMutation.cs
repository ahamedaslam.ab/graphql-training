﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;
using GraphQL.Demo1.Data.Repository;
using GraphQL.Demo1.GraphQL.Messages;
using GraphQL.Demo1.GraphQL.Types;
using GraphQL.Demo1.GraphQL.Validations;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL
{
    public class MyCommerceMutation : ObjectGraphType
    {
        public MyCommerceMutation(IProductRepository productRepository, ReviewMessageService messageService)
        {
            FieldAsync<ProductReviewType>(
               "createReview",
               arguments: new QueryArguments(
                   new QueryArgument<NonNullGraphType<ProductReviewInputType>> { Name = "review" }),

               resolve: async context =>
               {
                   var review = context.GetArgument<ProductReview>("review");

                   var validator = new ProductReviewValidator();

                   var validationResult = validator.Validate(review);

                   if (!validationResult.IsValid)
                   {
                       foreach (var error in validationResult.Errors)
                       {
                           context.Errors.Add(new ExecutionError(error.ErrorMessage));
                       }

                       return null;
                   }

                   //if (review.Title.Length > 50)
                   //{
                   //    context.Errors.Add(new ExecutionError("Title is invalid"));
                   //    return null;
                   //}

                   await productRepository.AddReview(review);
                   messageService.AddReviewAddedMessage(review);
                   return review;
               });
        }
    }
}
