﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Entities;
using Microsoft.Extensions.Logging;

namespace GraphQL.Demo1.Data.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ILogger logger;

        public ProductRepository(ILogger<ProductRepository> logger)
        {
            this.logger = logger;
        }

        public Task<ProductReview> AddReview(ProductReview review)
        {
            review.Id = new Random().Next(1, 1000000);
            review.Review = $"New product review added for product - {review.ProductId}";

            return Task.FromResult(review);
        }

        public Task<List<Product>> GetAll()
        {

            return Task.FromResult(new List<Product>
            {
                new Product{ Id= 1001,
                             Name ="Product 1",
                             Description = "This is the description 1 ",
                             IntroducedAt = DateTime.Now.AddDays(-100),
                             PhotoFileName ="product1.jpg",
                             Price =100.00m,
                             Rating = 4,
                             Stock =100,
                             Type = ProductType.Boots },
                 new Product{ Id= 1002,
                             Name ="Product 2",
                             Description = "This is the description 2 ",
                             IntroducedAt = DateTime.Now.AddDays(-80),
                             PhotoFileName ="product2.jpg",
                             Price =80.00m,
                             Rating = 3,
                             Stock =100,
                             Type = ProductType.ClimbingGear },
                 new Product{ Id= 1003,
                             Name ="Product 3",
                             Description = "This is the description 3 ",
                             IntroducedAt = DateTime.Now.AddDays(-5),
                             PhotoFileName ="product3.jpg",
                             Price =1000.00m,
                             Rating = 4,
                             Stock =80,
                             Type = ProductType.Kayaks },
                  new Product{ Id= 1004,
                             Name ="Product 4",
                             Description = "This is the description 4 ",
                             IntroducedAt = DateTime.Now.AddDays(-100),
                             PhotoFileName ="product4.jpg",
                             Price =100.00m,
                             Rating = 4,
                             Stock =100,
                             Type = ProductType.Boots },
                 new Product{ Id= 1005,
                             Name ="Product 5",
                             Description = "This is the description 5 ",
                             IntroducedAt = DateTime.Now.AddDays(-80),
                             PhotoFileName ="product5.jpg",
                             Price =80.00m,
                             Rating = 3,
                             Stock =100,
                             Type = ProductType.ClimbingGear },
                 new Product{ Id= 1006,
                             Name ="Product 6",
                             Description = "This is the description 6 ",
                             IntroducedAt = DateTime.Now.AddDays(-5),
                             PhotoFileName ="product6.jpg",
                             Price =1000.00m,
                             Rating = 4,
                             Stock =80,
                             Type = ProductType.Kayaks },
            });
        }

        public async Task<Product> GetProductById(int id)
        {
            var products = await this.GetAll();

          
            return products.FirstOrDefault(x => x.Id == id);
        }

        public Task<List<ProductReview>> GetReviewsByProductId(int productId)
        {
            //logger.LogInformation($"Calling GetReviewsByProductId with {productId}");
            return Task.FromResult(new List<ProductReview>
            {
                new ProductReview { Id = 1, ProductId = productId, Title = $"Review for product {productId} - 1", Review = "Awesome product" },
                new ProductReview { Id = 1, ProductId = productId, Title = $"Review for product {productId} - 2", Review = "Awesome product" },
                new ProductReview { Id = 1, ProductId = productId, Title = $"Review for product {productId} - 3", Review = "Awesome product" }

            }
            );
        }

        public async Task<ILookup<int, ProductReview>> GetReviewsByProductIds(IEnumerable<int> productIds)
        {
            logger.LogInformation($"Calling GetReviewsByProductIds with {productIds.Count()}");
            var reviews = new List<ProductReview>();

            foreach (var producId in productIds)
            {
                reviews.AddRange(await this.GetReviewsByProductId(producId));
            }
            return reviews.ToLookup(r => r.ProductId);
        }

        public Task<List<SpecialProduct>> GetSpecialProducts()
        {
            return Task.FromResult(new List<SpecialProduct>
            {
                new SpecialProduct{ Id= 1001,
                             Name ="Product 1",
                             Description = "This is the description 1 ",
                             IntroducedAt = DateTime.Now.AddDays(-100),
                             PhotoFileName ="product1.png",
                             Price =100.00m,
                             Rating = 4,
                             Stock =100,
                             Type = ProductType.Boots,
                            Size = 8},
                 new SpecialProduct{ Id= 1002,
                             Name ="Product 2",
                             Description = "This is the description 2 ",
                             IntroducedAt = DateTime.Now.AddDays(-80),
                             PhotoFileName ="product2.png",
                             Price =80.00m,
                             Rating = 3,
                             Stock =100,
                             Type = ProductType.ClimbingGear,
                             Size = 10},
                 new SpecialProduct{ Id= 1003,
                             Name ="Product 3",
                             Description = "This is the description 3 ",
                             IntroducedAt = DateTime.Now.AddDays(-5),
                             PhotoFileName ="product3.png",
                             Price =1000.00m,
                             Rating = 4,
                             Stock =80,
                             Type = ProductType.Kayaks,
                             Size = 10},
            });
        }
    }
}
