﻿using GraphQL.Demo1.GraphQL.Messages;
using GraphQL.Types;

namespace GraphQL.Demo1.Api.GraphQL.Types
{
    public class ReviewAddedMessageType: ObjectGraphType<ReviewAddedMessage>
    {
        public ReviewAddedMessageType()
        {
            Field(t => t.ProductId);
            Field(t => t.Title);
        }
    }
}
