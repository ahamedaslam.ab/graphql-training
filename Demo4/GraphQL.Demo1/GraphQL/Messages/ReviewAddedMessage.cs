﻿namespace GraphQL.Demo1.GraphQL.Messages
{
    public class ReviewAddedMessage
    {
        public int ProductId { get; set; }
        public string Title { get; set; }
    }
}
