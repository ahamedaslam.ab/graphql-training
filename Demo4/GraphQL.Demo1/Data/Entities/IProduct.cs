﻿using System;

namespace GraphQL.Demo1.Data.Entities
{
    public interface IProduct
    {
        string Description { get; set; }
        int Id { get; set; }
        DateTimeOffset IntroducedAt { get; set; }
        string Name { get; set; }
        string PhotoFileName { get; set; }
        decimal Price { get; set; }
        int Rating { get; set; }
        int Stock { get; set; }
        ProductType Type { get; set; }
    }
}