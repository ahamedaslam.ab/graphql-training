﻿namespace MyCommerce.Web.Models
{
    public enum ProductTypeEnum
    {
        Boots,
        ClimbingGear,
        Kayaks
    }
}
