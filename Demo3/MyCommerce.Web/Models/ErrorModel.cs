﻿namespace MyCommerce.Web.Models
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public string Code { get; set; }
    }
}
