﻿using System;

namespace MyCommerce.Web
{
    public class GraphQlException: ApplicationException
    {
        public GraphQlException(string message): base(message)
        {
        }
    }
}
