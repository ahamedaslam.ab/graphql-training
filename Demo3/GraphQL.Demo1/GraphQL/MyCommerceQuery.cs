﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Demo1.Data.Repository;
using GraphQL.Demo1.GraphQL.Types;
using GraphQL.Types;

namespace GraphQL.Demo1.GraphQL
{
    public class MyCommerceQuery : ObjectGraphType
    {
        public MyCommerceQuery(IProductRepository productRepository)
        {
            Field<ListGraphType<ProductType>>(
                "products",
                resolve: context => productRepository.GetAll()
            );

            //Field<ListGraphType<IProductType>>(
            //    "products",
            //    resolve: context => productRepository.GetAll()
            //);

            //Field<ListGraphType<SpecialProductType>>(
            //    "specials",
            //    resolve: context => productRepository.GetSpecialProducts());

            Field<ProductType>(
                "product",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>>
                { Name = "id" }),
                resolve: context =>
                {
                    var id = context.GetArgument<int>("id");
                    return productRepository.GetProductById(id);
                }
            );
        }
    }
}
