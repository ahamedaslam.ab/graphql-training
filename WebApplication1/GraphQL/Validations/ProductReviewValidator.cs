﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using GraphQL.Demo1.Data.Entities;

namespace GraphQL.Demo1.GraphQL.Validations
{
    public class ProductReviewValidator : AbstractValidator<ProductReview>
    {
        public ProductReviewValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title is required").MaximumLength(50).WithMessage("Title should not exceed 50 charactors length");
        }
    }
}
